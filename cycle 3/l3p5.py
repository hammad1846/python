#Write a program to print the Fibonacci series using recursion.
def rec(n):
 if n<=1:
  return(n)
 else:
   return(rec(n-1)+rec(n-2))
n=int(input("Enter number of terms:"))
if(n<1):
 print("!!!!Enter a positive number")
else:
 print("FIBONACCI SERIES")
 for i in range(n):
  print(rec(i)) 
